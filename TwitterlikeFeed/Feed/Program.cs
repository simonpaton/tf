﻿using TF.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TF.Feed
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Dictionary<string, string> parameters = GetParametersFromArguments(args);

            if (parameters.ContainsKey("-u") && parameters.ContainsKey("-t"))
            {
                IContentProvider contentProvider = new ContentProvider(
                    pathToUsers: parameters["-u"],
                    pathToTweets: parameters["-t"]);
                IContentEngine processor = new ContentEngine();
                IFeedPresenter presenter = new FeedPresenter();

                try
                {
                    IEnumerable<TwitterUser> users = processor.GetUsers(contentProvider.GetUsersContent());
                    IEnumerable<Tweet> tweets = processor.GetTweets(contentProvider.GetTweetsContent());
                    IEnumerable<FeedEntry> feed = processor.GetFeed(users, tweets);

                    presenter.RenderFeed(feed);
                }
                catch(Exception ioe)
                {
                    Console.WriteLine(string.Format("An error occured when retrieving the feed/content: {0}", ioe.Message));
                }
            }
            else
            {
                Console.WriteLine("Usage: -u (path to users file), -t (path to tweets file)");
            }
        }

        private static Dictionary<string, string> GetParametersFromArguments(string[] args)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            string currentKey = string.Empty;

            if (args != null)
            {
                for (int index = 0; index < args.Length; index++)
                {
                    if (args[index].StartsWith("-"))
                    {
                        parameters.Add(args[index], string.Empty);
                        currentKey = args[index];
                    }
                    else if (parameters.ContainsKey(currentKey))
                    {
                        parameters[currentKey] += args[index];
                    }
                }
            }

            return parameters;
        }
    }
}
