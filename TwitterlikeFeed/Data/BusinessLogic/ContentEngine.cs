﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace TF.Data
{
    public class ContentEngine : IContentEngine
    {
        public IEnumerable<TwitterUser> GetUsers(string[] input)
        {
            string regexFormat = @"([A-Z][a-z]+)\sfollows\s(.*)";
            Regex rx = new Regex(regexFormat);
            List<TwitterUser> users = new List<TwitterUser>();
            TwitterUser user = null;
            Match match = null;
            string name = null;
            bool isNew = false;

            if (input != null)
            {

                foreach (string line in input)
                {
                    match = rx.Match(line);
                    if (match.Groups.Count > 2)
                    {
                        name = match.Groups[1].Value;
                        user = users.FirstOrDefault(u => u.Name.Equals(name));
                        isNew = user == null;
                        user = user ?? new TwitterUser() { Name = name };

                        user.Followers = user.Followers.Union(match.Groups[2].Value
                            .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                            .Select(f => f.Trim())
                            .ToList());

                        if (isNew)
                        {
                            users.Add(user);
                        }
                    }

                    isNew = false;
                    user = null;
                    match = null;
                    name = null;
                }

                List<TwitterUser> newUsersFromFollowers = users
                    .SelectMany(u => u.Followers)
                    .Distinct()
                    .Where(f => users.FirstOrDefault(usr => usr.Name.Equals(f)) == null)
                    .Select(f => new TwitterUser { Name = f })
                    .ToList();

                users.AddRange(newUsersFromFollowers);
            }

            return users;
        }

        public IEnumerable<Tweet> GetTweets(string[] input)
        {
            string regexFormat = @"([A-Z][a-z]+)>\s(.*)";
            Regex rx = new Regex(regexFormat);
            List<Tweet> tweets = new List<Tweet>();
            Tweet tweet = null;
            Match match = null;

            if (input != null)
            {

                foreach (string line in input)
                {
                    tweet = new Tweet();
                    match = rx.Match(line);
                    if (match.Groups.Count > 2)
                    {
                        tweet.User = match.Groups[1].Value;
                        tweet.Message = match.Groups[2].Value;
                    }

                    if (tweet.IsValid)
                    {
                        tweets.Add(tweet);
                    }

                    tweet = null;
                    match = null;
                }
            }

            return tweets;
        }

        public IEnumerable<FeedEntry> GetFeed(IEnumerable<TwitterUser> users, IEnumerable<Tweet> tweets)
        {
            return users
                .OrderBy(u => u.Name)
                .ToList()
                .Select(u => new FeedEntry()
                {
                    User = u,
                    Tweets = tweets.Where(t => t.User.Equals(u.Name) || u.Followers.Contains(t.User)).ToList()
                })
                .ToList();
        }
    }
}
