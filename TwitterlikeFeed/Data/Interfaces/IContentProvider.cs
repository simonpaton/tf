﻿namespace TF.Data
{
    public interface IContentProvider
    {
        string[] GetTweetsContent();
        string[] GetUsersContent();
    }
}