﻿using System.Collections.Generic;

namespace TF.Data
{
    public interface IFeedPresenter
    {
        void RenderFeed(IEnumerable<FeedEntry> feed);
    }
}