﻿using System.Collections.Generic;

namespace TF.Data
{
    public interface IContentEngine
    {
        IEnumerable<FeedEntry> GetFeed(IEnumerable<TwitterUser> users, IEnumerable<Tweet> tweets);
        IEnumerable<Tweet> GetTweets(string[] input);
        IEnumerable<TwitterUser> GetUsers(string[] input);
    }
}