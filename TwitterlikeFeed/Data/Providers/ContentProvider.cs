﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TF.Data
{
    public class ContentProvider : IContentProvider
    {
        public string PathToUsers { get; private set; }
        public string PathToTweets { get; private set; }

        public ContentProvider(string pathToUsers, string pathToTweets)
        {
            PathToUsers = pathToUsers;
            PathToTweets = pathToTweets;
        }

        public string[] GetUsersContent()
        {
            return File.ReadAllLines(PathToUsers);
        }

        public string[] GetTweetsContent()
        {
            return File.ReadAllLines(PathToTweets);
        }
    }
}
