﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TF.Data
{
    public class FeedPresenter : IFeedPresenter
    {
        public void RenderFeed(IEnumerable<FeedEntry> feed)
        {
            if (feed != null && feed.Count() > 0)
            {
                foreach (FeedEntry feedEntry in feed)
                {
                    Console.WriteLine(string.Format("{0}", feedEntry.User.Name));
                    foreach (Tweet tweet in feedEntry.Tweets)
                    {
                        Console.WriteLine(string.Format("\t@{0}: {1}", tweet.User, tweet.Message));
                    }
                }
            }
            else
            {
                Console.WriteLine("No feed/content to display.");
            }
        }
    }
}