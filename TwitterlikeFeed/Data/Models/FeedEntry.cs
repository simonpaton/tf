﻿using System.Collections;
using System.Collections.Generic;

namespace TF.Data
{
    public class FeedEntry
    {
        public TwitterUser User { get; set; }
        public IEnumerable<Tweet> Tweets { get; set; }
    }
}