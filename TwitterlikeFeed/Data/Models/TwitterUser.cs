﻿using System.Collections.Generic;

namespace TF.Data
{
    public class TwitterUser
    {
        public string Name { get; internal set; }
        public IEnumerable<string> Followers { get; set; }

        public TwitterUser()
        {
            Followers = new List<string>();
        }
    }
}