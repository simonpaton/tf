﻿using System.Text.RegularExpressions;

namespace TF.Data
{
    public class Tweet
    {
        public bool IsValid
        {
            get
            {
                return !string.IsNullOrEmpty(Message) 
                    && !string.IsNullOrEmpty(User);
            }
        }
        public string Message { get; internal set; }
        public string User { get; internal set; }
    }
}