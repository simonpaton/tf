﻿// Create Module for Controllers
angular.module('TwitterFeedAppControllers', []);

// UserCtrl
(function () {
    'use strict';

    angular
        .module('TwitterFeedAppControllers')
        .controller('UserCtrl', controller);

    controller.$inject = ['$scope', '$http', '$routeParams', '$location'];

    function controller($scope, $http, $routeParams, $location) {
        $scope.title = 'Users';

        init();

        function init() {
            $http.get('api/user')
                .then(onLoad)
                .catch(onError);
        }

        function onLoad(resp) {
            if (resp && resp.data) {
                $scope['users'] = resp.data;
            }
        }

        function onError(err) {
            console.log('err');
            console.log(err);
        }
    }
})();

// TweetCtrl
(function () {
    'use strict';

    angular
        .module('TwitterFeedAppControllers')
        .controller('TweetCtrl', controller);

    controller.$inject = ['$scope', '$http', '$routeParams', '$location'];

    function controller($scope, $http, $routeParams, $location) {
        $scope.title = 'Tweets';

        init();

        function init() {
            $http.get('api/tweet')
                .then(onLoad)
                .catch(onError);
        }

        function onLoad(resp) {
            if (resp && resp.data) {
                $scope['tweets'] = resp.data;
            }
        }

        function onError(err) {
            console.log('Please see error log below');
            console.log(err);
        }
    }
})();

// FeedCtrl
(function () {
    'use strict';

    angular
        .module('TwitterFeedAppControllers')
        .controller('FeedCtrl', controller);

    controller.$inject = ['$scope', '$http', '$routeParams', '$location'];

    function controller($scope, $http, $routeParams, $location) {
        $scope.title = 'Combined Feed';

        init();

        function init() {
            $http.get('api/feed')
                .then(onLoad)
                .catch(onError);
        }

        function onLoad(resp) {
            if (resp && resp.data) {
                $scope['feed'] = resp.data;
            }
        }

        function onError(err) {
            console.log('Please see error log below');
            console.log(err);
        }
    }
})();