﻿var TwitterFeedApp = angular.module('TwitterFeedApp', [
    'ngRoute',
    'TwitterFeedAppControllers'
]);

TwitterFeedApp.config(['$routeProvider',
function ($routeProvider) {
    $routeProvider
        .when('/feeds', {
            templateUrl: 'content/feeds.html'
        })
        .when('/users', {
            templateUrl: 'content/users.html',
            controller: 'UserCtrl'
        })
        .when('/tweets', {
            templateUrl: 'content/tweets.html',
            controller: 'TweetCtrl'
        })
        .when('/combinedfeed', {
            templateUrl: 'content/combinedfeed.html',
            controller: 'FeedCtrl'
        })
        .otherwise({
            redirectTo: '/feeds'
        });
}]);