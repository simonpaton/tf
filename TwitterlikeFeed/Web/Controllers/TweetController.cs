﻿using TF.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web.Controllers
{
    public class TweetController : BaseApiController
    {
        public TweetController(IContentProvider provider, IContentEngine engine) : base(provider, engine) { }

        public object Get()
        {
            System.Threading.Thread.Sleep(new Random((int)DateTime.Now.Ticks).Next(1000));
            try
            {
                var users = Engine.GetTweets(Provider.GetTweetsContent());
                return Json(users, JsonSettings);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
