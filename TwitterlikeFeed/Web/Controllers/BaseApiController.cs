﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TF.Data;

namespace Web.Controllers
{
    public class BaseApiController : ApiController
    {
        protected IContentEngine Engine;
        protected IContentProvider Provider;
        protected JsonSerializerSettings JsonSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        public BaseApiController(IContentProvider provider, IContentEngine engine)
        {
            Provider = provider;
            Engine = engine;
        }
    }
}