﻿using TF.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Web.Helpers
{
    public class WebContentProvider : ContentProvider
    {
        public WebContentProvider(string pathToUsers, string pathToTweets) : base(pathToUsers, pathToTweets)
        {
        }

        public WebContentProvider() : 
            this(
                pathToUsers: ConfigurationManager.AppSettings["pathToUsers"],
                pathToTweets: ConfigurationManager.AppSettings["pathToTweets"])
        {
        }
    }
}