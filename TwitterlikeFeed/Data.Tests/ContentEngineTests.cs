﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TF.Data.Tests
{
    [TestFixture]
    public class ContentEngineTests
    {
        private IContentEngine Engine { get; set; }
        public IEnumerable<TwitterUser> Users { get; private set; }
        public IEnumerable<Tweet> ValidTweets { get; private set; }
        public IEnumerable<Tweet> InvalidTweets { get; private set; }

        [SetUp]
        public void SetUp()
        {
            Engine = new ContentEngine();
            Users = Engine.GetUsers(new string[]
            {
                "Ward follows Alan",
                "Alan follows Martin",
                "Ward follows Martin, Alan"
            });

            ValidTweets = Engine.GetTweets(new string[]
            {
                "Alan> If you have a procedure with 10 parameters, you probably missed some.",
                "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.",
                "Alan> Random numbers should not be generated with a method chosen at random."
            });

            InvalidTweets = Engine.GetTweets(new string[]
            {
                "Alan> If you have a procedure with 10 parameters, you probably missed some.",
                "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.",
                "Alan > Random numbers should not be generated with a method chosen at random."
            });
        }

        [Test]
        public void GetUsersIsNotNull()
        {
            Assert.IsNotNull(Users);
        }

        [Test]
        public void GetUsersHasThreeUsers()
        {
            Assert.AreEqual(3, Users.Count());
        }

        [Test]
        public void GetUsersHasFollowers()
        {
            TwitterUser alan = Users.First(user => user.Name.Equals("Alan"));

            Assert.IsTrue(alan.Followers.Count() > 0);
        }

        [Test]
        public void GetUsersHasUniqueFollowers()
        {
            TwitterUser alan = Users.First(user => user.Name.Equals("Ward"));

            Assert.AreEqual(2, alan.Followers.Count());
        }

        [Test]
        public void GetTweetsIsNotNull()
        {
            Assert.IsNotNull(ValidTweets);
        }

        [Test]
        public void GetTweetsExcludesInvalidTweets()
        {
            Assert.AreEqual(2, InvalidTweets.Count());
        }

        [Test]
        public void GetFeedIsNotNull()
        {
            IEnumerable<FeedEntry> feed = Engine.GetFeed(Users, ValidTweets);

            Assert.IsNotNull(feed);
        }

        [Test]
        public void GetFeedHasThreeEntriesAndFiveTweets()
        {
            IEnumerable<FeedEntry> feed = Engine.GetFeed(Users, ValidTweets);

            Assert.AreEqual(3, feed.Count());
            Assert.AreEqual(5, feed.SelectMany(f => f.Tweets).Count());
        }
    }
}
