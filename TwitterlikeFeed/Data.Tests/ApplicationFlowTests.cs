﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TF.Data.Tests
{
    [TestFixture]
    public class ApplicationFlowTests
    {
        public IContentProvider EmptyProvider { get; private set; }
        public ContentEngine Engine { get; private set; }
        public IContentProvider ValidProvider { get; private set; }
        public IContentProvider ExceptionProvider { get; private set; }

        [SetUp]
        public void SetUp()
        {
            Mock<IContentProvider> mockedProvider = new Mock<IContentProvider>();
            mockedProvider.Setup(p => p.GetUsersContent()).Returns(new string[]
            {
                "Ward follows Alan",
                "Alan follows Martin",
                "Ward follows Martin, Alan"
            });

            mockedProvider.Setup(p => p.GetTweetsContent()).Returns(new string[]
            {
                "Alan> If you have a procedure with 10 parameters, you probably missed some.",
                "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.",
                "Alan> Random numbers should not be generated with a method chosen at random."
            });

            ValidProvider = mockedProvider.Object;

            mockedProvider = null;
            mockedProvider = new Mock<IContentProvider>();
            mockedProvider.Setup(p => p.GetUsersContent()).Returns((string[])null);
            mockedProvider.Setup(p => p.GetTweetsContent()).Returns((string[])null);

            EmptyProvider = mockedProvider.Object;

            Engine = new ContentEngine();
        }

        [Test]
        public void GetUsersWithoutExceptionFromBrokenContentProvider()
        {
            Assert.DoesNotThrow(() =>
            {
                Engine.GetUsers(EmptyProvider.GetUsersContent());
            });
        }

        [Test]
        public void GetTweetsWithoutExceptionFromBrokenContentProvider()
        {
            Assert.DoesNotThrow(() =>
            {
                Engine.GetTweets(EmptyProvider.GetTweetsContent());
            });
        }
    }
}
