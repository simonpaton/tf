﻿using TF.Data;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Moq;

namespace Data.Tests
{
    [TestFixture]
    public class ContentProviderTests
    {
        private IContentProvider ValidProvider { get; set; }
        private IContentProvider BrokenProvider { get; set; }

        [SetUp]
        public void SetUp()
        {
            Mock<IContentProvider> mockedProvider = new Mock<IContentProvider>();
            mockedProvider.Setup(p => p.GetUsersContent()).Returns(new string[]
            {
                "Ward follows Alan",
                "Alan follows Martin",
                "Ward follows Martin, Alan"
            });

            mockedProvider.Setup(p => p.GetTweetsContent()).Returns(new string[]
            {
                "Alan> If you have a procedure with 10 parameters, you probably missed some.",
                "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.",
                "Alan> Random numbers should not be generated with a method chosen at random."
            });

            ValidProvider = mockedProvider.Object;
        }

        [Test]
        public void GetUsersContentIsNotNull()
        {
            string[] users = ValidProvider.GetUsersContent();
            Assert.IsNotNull(users);
        }

        [TestCase(@"[A-Z][a-z]+\sfollows\s[A-Z][a-z]+((,\s[A-Z][a-z]+)+)?")]
        public void GetUsersIsCorrectFormat(string regexFormat)
        {
            string[] users = ValidProvider.GetUsersContent();
            Regex matcher = new Regex(regexFormat);

            foreach(string line in users)
            {
                Assert.True(matcher.IsMatch(line));
            }
        }

        [Test]
        public void GetTweetsContentIsNotNull()
        {
            string[] tweets = ValidProvider.GetTweetsContent();
            Assert.IsNotNull(tweets);
        }

        [TestCase(@"[A-Z][a-z]+>\s(.){1,140}$")]
        public void GetTweetsIsCorrectFormat(string regexFormat)
        {
            string[] tweets = ValidProvider.GetTweetsContent();
            Regex matcher = new Regex(regexFormat);

            foreach (string line in tweets)
            {
                Assert.True(matcher.IsMatch(line));
            }
        }
    }
}
