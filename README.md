# README #

This is a console and web app that displays a twitter-like feed.

### How do I get set up? ###

* Open and build the solution in Visual Studio 2015

#### Console app ####
* Run TF.Feed.exe -u "path to users.txt" -t "path to tweets.txt"

#### Web app/api ####
* See README in Web for more details regarding file access
* Once built, will be found at http://localhost/tfweb/
* User API http://localhost/tfweb/api/user
* Tweet API http://localhost/tfweb/api/tweet
* Feed API http://localhost/tfweb/api/feed